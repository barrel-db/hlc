

# hlc - Hybrid Logical Clock in Erlang. #

Copyright (c) 2014-2015 Benoît Chesneau.

__Version:__ 3.0.1

hlc implements the Hybrid Logical Clock outlined in [Logical Physical Clocks
and Consistent Snapshots in Globally Distributed
Databases](http://www.cse.buffalo.edu/tech-reports/2014-04.pdf).

> Note: you can use it to have timestamps that are are a combination of both a
> physical and a logical component to support monotonic increments without
> degenerate cases causing timestamps to diverge from wall clock time. It's
> usefull to distribute transactions or such things.

[![Build Status](https://gitlab.com/barrel-db/hlc/badges/master/build.svg)](https://gitlab.com/barrel-db/hlc/commits/master)
[![Hex pm](http://img.shields.io/hexpm/v/hlc.svg?style=flat)](https://hex.pm/packages/hlc)

## Documentation

Full doc is available in the [`hlc`](http://gitlab.com/barrel-db/hlc/blob/master/doc/hlc.md) module.

## Example of usage

Create a logical clock using `hlc:new/0`:

```
1> {ok, C} = hlc:start_link().
{ok,<0.158.0>}
```
Return a timestamp for the current time:

```
2> Now = hlc:now(C).
{timestamp,1511564016030,0}
```

> Note: by default it using `erlang:system_time(millisecond)` to get the physical time.

You can update the current clock from the members of the cluster using `hlc:update/2`.

> :heavy_exclamation_mark: A clock is not locked, you need to make sure that only one user can update it at a time
> using the `now/1` or `update/2` functions.

### Comparaison

Compare 2 clocks using `hlc:ts_less/2` or `hlc:ts_equal/2`:

Ex, compare if A is inferior to B:

```
{MClock, MClockFun} = hlc:manual_clock(),
{ok, C} = hlc:start_link(MClockFun, 0),

A = hlc:timestamp(C),
B = hlc:timestamp(C),

?assert(A =:= B),

hlc:set_manual_clock(MClock, 1),
B1 = hlc:now(C),
true = hlc:less(A, B1).
```

To test if they are equal use `hlc:ts_equal/2`.

## Performance

You can check the performance using the module `hlc_harness`:

```
1> hlc_harness:timed_generate(10000).
generating timestamp: 0.035 s
...
2> hlc_harness:timed_generate(100000).
generating timestamp: 0.295 s
...
3> hlc_harness:timed_generate(1000000).
generating timestamp: 2.586 s
```

## Ownership and License

The contributors are listed in AUTHORS. This project uses the MPL v2
license, see LICENSE.

hlc uses the [C4.1 (Collective Code Construction
Contract)](http://rfc.zeromq.org/spec:22) process for contributions.

## Development

Under C4.1 process, you are more than welcome to help us by:

* join the discussion over anything from design to code style try out
* and [submit issue reports](https://github.com/refuge/hlc/issues/new)
* or feature requests pick a task in
* [issues](https://github.com/refuge/hlc/issues) and get it done fork
* the repository and have your own fixes send us pull requests and even
* star this project ^_^

To  run the test suite:

```
rebar3 eunit
```



## Modules ##


<table width="100%" border="0" summary="list of modules">
<tr><td><a href="http://gitlab.com/barrel-db/hlc/blob/master/doc/hlc.md" class="module">hlc</a></td></tr>
<tr><td><a href="http://gitlab.com/barrel-db/hlc/blob/master/doc/hlc_harness.md" class="module">hlc_harness</a></td></tr></table>

